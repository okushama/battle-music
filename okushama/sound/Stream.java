package okushama.sound;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Control;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.FloatControl.Type;

import okushama.battlemusic.VolumeSlider;
import okushama.battlemusic.core.ProxyClient;
import okushama.battlemusic.core.TickerClient;


import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class Stream implements Runnable{
	
	public String modid, streamid;
	public String lastFile;
	public float volume = 0f;
	public boolean loop = false;
	
	public Stream(String m, String s){
		System.out.println("Stream Created: "+m+" - "+s);
		modid = m;
		streamid = s;
		new Thread(this).start();
	}
	
	@Override
	public void run(){
		while(true){
			try{
				if(mainMusic.isFinished && loop){
					play(lastFile, volume, false, false);
				}
				Thread.sleep(20);
			}catch(Exception e){
				
			}
		}
	}
	


	public Music mainMusic;
	
	
	//public static float masterVolume = 20;
	
	
	public boolean stop(boolean fade){
		if(mainMusic!= null){
			loop = false;
			if(!fade){
				mainMusic.stopMusic();
			}else{
				if(mainMusic.isFinished){
					return false;
				}
				new AudioDelay.Wait(20,50, new AudioDelay.Callback() {
					
					@Override
					public void execute() {
						mainMusic.stopMusic();
					}
					
					@Override
					public void count() {
						//System.out.println(mainMusic.volume+"!");
						if(volume > -80){
							//mainMusic.volume-=0.25f;
						}
					}
				});
			}
			return true;
		}
		return false;
	}


	
	public boolean play(String file, float vol, boolean fadeIn, boolean fadeOut)
	{
		//System.out.println("Trying!");
		if(mainMusic == null || mainMusic.isFinished){
			try {
				ResourceLocation rl = new ResourceLocation(modid+":sound/"+file);
				BufferedInputStream in = new BufferedInputStream(Minecraft.getMinecraft().getResourceManager().getResource(rl).getInputStream());
				mainMusic = new Music(in, this);
				lastFile = file;
				return true;
			} catch (Exception e) {
				System.out.println("okusound api could not play stream!");
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}
}
