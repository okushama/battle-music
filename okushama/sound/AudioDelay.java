package okushama.sound;


public class AudioDelay {
	public static class Wait implements Runnable{

		public boolean isWaiting = true;
		public boolean finished = false;
		public int tickDelay, totalTicks;
		public Callback after;
		public Wait(int tD, int tT, Callback a){
			tickDelay = tD;
			totalTicks = tT;
			after = a;
			new Thread(this).start();
		}
		
		@Override
		public void run() {
			while(isWaiting){
				after.count();
				totalTicks--;
				if(totalTicks <= 0){
					isWaiting = false;
					finished = true;
					after.execute();
				}
				try{
					Thread.sleep(tickDelay);
				}catch(Exception e){
					
				}
			}
		}
		
	}
	
	public static interface Callback{
		public void execute();
		public void count();
	}
}
