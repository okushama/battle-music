package okushama.sound;

import java.util.HashMap;



public class SoundAPI{
	public static float MASTER_VOLUME = 0f;
	private static SoundAPI instance;
	public static SoundAPI getInstance(){
		if(instance == null){
			instance = new SoundAPI();
		}
		return instance;
	}
	
	public HashMap<String, Stream> streams = new HashMap<String, Stream>();
	public HashMap<String, HashMap> modStreams = new HashMap<String, HashMap>();
	
	/**
	 * Creates and/or returns an instance of a Stream.
	 * @param modid - Unique identifier
	 * @param id - Stream name
	 * @return okushama.sound.Stream
	 */
	public Stream getStream(String modid, String id){
		if(modStreams.get(modid) == null){
			modStreams.put(modid, new HashMap<String, Stream>());
			System.out.println("okusound api - registered mod: "+modid);
		}

		if(streams.get(id) == null){
			Stream newStream = new Stream(modid, id);
			streams.put(id, newStream);
			modStreams.get(modid).put(id, newStream);

		}
		return streams.get(id);
	}
	

	public SoundAPI(){
		System.out.println("okusound api initiating!");
	}
	
}