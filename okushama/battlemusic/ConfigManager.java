package okushama.battlemusic;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipFile;

import okushama.battlemusic.plugin.PVPTracker;

import org.apache.commons.lang3.Validate;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.Resource;
import net.minecraft.client.resources.SimpleResource;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.Configuration;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public class ConfigManager {
	
	
	public void ejectInitialPacks(){
		InputStream stream = ConfigManager.class.getResourceAsStream("default.zip");
	    if (stream == null) {
	        //send your exception or warning
	    }
	    OutputStream out;
	    int readBytes;
	    byte[] buffer = new byte[4096];
	    try {
	        out = new FileOutputStream(new File(Helper.getMinecraftDir("battlemusic"), "default.zip"));
	        while ((readBytes = stream.read(buffer)) > 0) {
	            out.write(buffer, 0, readBytes);
	        }
	        out.close();
	        System.out.println("Created default pack in .minecraft/battlemusic/!");
	    } catch (IOException e1) {
	        e1.printStackTrace();
	    }
	   
	}
	
	public void loadThemes(){
		themes = new ArrayList<Theme>();
		try{
			File f = Helper.getMinecraftDir("battlemusic");
			if(!f.exists()){
				f.mkdir();
				ejectInitialPacks();
			}
			for(File fi : f.listFiles()){
				if(fi.getName().contains(".zip")){
					ZipFile zip = new ZipFile(fi);
					BufferedReader in = new BufferedReader(new InputStreamReader(zip.getInputStream(zip.getEntry("theme.okumeta"))));
					String out = "";
					String t = "";
					while((t = in.readLine()) != null){
						out += t+"\u00A7";
					}
					out = out.substring(0, out.length()-1);
					/*System.out.println("------------------------------------");
					*/String[] outargs = out.split("\u00A7");/*
					System.out.println("Pack Name: "+outargs[0]);
					if(outargs.length > 1){
						System.out.println("Author: "+outargs[1]);
						if(outargs.length > 2){
							System.out.println("Website: "+outargs[2]);
						}
					}*/
					in.close();
					themes.add(new Theme(fi.getAbsolutePath(), outargs));
					//zip.close();
				}
			}
			}catch(Exception e){
				e.printStackTrace();
			}
		themes.add(new ThemesGetMore());
	}
	
	public ArrayList<Theme> themes = new ArrayList<Theme>();
	public ConfigManager(){
		loadThemes();
		
	}

	public Configuration getConfig(){
		return config;
	}
	
	public Configuration config;
	public void init(FMLPreInitializationEvent event){
		config = new Configuration(event.getSuggestedConfigurationFile());
		loadConfig();
		GuiBattleMusicNew.currentSelection = currentPack;
		maxVolume = (float)config.get("auto", "master", 80D).getDouble(80D);
	}
	
	public void loadConfig(){
		config.addCustomCategoryComment("auto", "Better off change this in-game...");
		ignoreDragonDistance = config.get("main", "ignoreDragonDistance", false).getBoolean(false);
		mobsToPlayTo = config.get("main", "affectedMobs", getDefaults()).getStringList();
		//biomesList = config.get("biome", "affectedBiomes", getDefaultBiomes()).getStringList();
	//	biomeThemes = config.get("biome", "affectedBiomeThemes", getDefaultBiomeTracks()).getStringList();
		currentPack = config.get("main", "currentPack", 0).getInt();
		showFightOverlay = config.get("main", "showFightOverlay", false).getBoolean(false);
		config.load();
		config.save();
	}
	
	public static String[] getDefaults(){
		return new String[]{"Creeper", "Skeleton", "Spider", "Zombie", "Slime","Ghast",
				"PigZombie", "Enderman", "CaveSpider","Silverfish", "Blaze", "LavaSlime",
				"Witch", "EnderDragon", "WitherBoss"};
	}
	
	public static String[] getDefaultBiomes(){
		return new String[]{"Ocean", "Plains", "Desert", "Extreme Hills", "Forest", "Taiga", "Swampland",
							"River", "Hell", "Sky", "FrozenOcean", "FrozenRiver", "Ice Plains",
							"Ice Mountains", "MushroomIsland", "MushroomIslandShore", "Beach","DesertHills",
							"ForestHills", "TaigaHills", "Extreme Hills Edge", "Jungle", "JungleHills"};
	}
	
	public static HashMap<String, String> biomeThemeFile(){
		if(biomeThemeMap.isEmpty()){
			for(int i =0; i < biomesList.length; i++){
				biomeThemeMap.put(biomesList[i], biomeThemes[i]);
			}
		}
		return biomeThemeMap;
	}
	
	private static 	HashMap<String, String> biomeThemeMap = new HashMap<String, String>();

	
	public static String[] getDefaultBiomeTracks(){
		String[] out = new String[getDefaultBiomes().length];
		for(int i =0; i < out.length; i++){
			out[i] = "default.ogg";
		}
		return out;
	}
	
	public static boolean containsMob(String s){
		if(s == null){
			return false;
		}
		for(String st : mobsToPlayTo){
			if(s.equals(st)){
				return true;
			}
		}
		return false;
	}
	
	private static String[] biomesList;
	private static String[] biomeThemes;
	private static String[] mobsToPlayTo;
	public static int currentPack;
	public boolean ignoreDragonDistance;
	public boolean showFightOverlay;
	
	public float maxVolume;
	
	public float getMaxVolume(){
		return maxVolume - 80f;
	}
}
