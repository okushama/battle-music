package okushama.battlemusic;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import com.google.common.io.Files;

import net.minecraft.util.ResourceLocation;

public class Theme {
	
	
	public String location = "", name = "", baseDir = "";
	public String[] attributes;
	public boolean useAttributePrefix = true;
	
	public Enumeration<? extends ZipEntry> entries;
	public ZipFile zipFile;
	public File[] files;
	public InputStream packImage;
	public ResourceLocation image;
	public Theme(String l, String[] attr){
		if(l.length() > 0){
			location = l;
			String splitter = File.separator.equals("\\") ? File.separator+File.separator : File.separator;
			baseDir = new File(l).getName();
			try{
				zipFile = new ZipFile(location);
				entries = zipFile.entries();
				ZipEntry zipimg = zipFile.getEntry("pack.png");
				packImage = zipFile.getInputStream(zipimg);
			//	zipFile.close();

			}catch(Exception e){
				e.printStackTrace();
			}
			image = new ResourceLocation("battlemusic:"+baseDir+"/"+"pack.png");
		}
		name = attr[0];
		attributes = attr;
	}

}
