package okushama.battlemusic.plugin;

import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.world.World;

public interface IPlugin {

	public void onModInit(FMLPreInitializationEvent evt);
	
	public void onClientTick(Minecraft mc, World w);
	
	public void onRenderTick(Minecraft mc, World w, float partialTick);
	
	public String pluginName();
	
	public void loadConfig();
	
	public void updateConfig();
	
	public boolean breakCoreLoop();
	
	public boolean isEnabled();
	
}
