package okushama.battlemusic.plugin;

import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public abstract class PluginBase implements IPlugin{

	public boolean enabled = true;
	public boolean breaksCoreLoop = false;
	
	public PluginBase(){
		
	}
	
	@Override
	public void onModInit(FMLPreInitializationEvent evt) {
		
	}
	
	@Override
	public void onRenderTick(Minecraft mc, World w, float partialTick){
		
	}

	@Override
	public void onClientTick(Minecraft mc, World w) {
		
	}

	@Override
	public void loadConfig() {

	}

	@Override
	public void updateConfig() {

	}

	@Override
	public boolean breakCoreLoop() {
		return breaksCoreLoop && enabled;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public void setState(boolean enabled) {
		this.enabled = enabled;
		System.out.println("[Battle Music "+this.pluginName()+" Plugin]: "+(enabled ? "Enabled" : "Disabled"));
	}

}
