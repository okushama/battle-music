package okushama.battlemusic.plugin;

import okushama.battlemusic.core.ProxyClient;
import static org.lwjgl.opengl.GL11.*;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class FightOverlay extends PluginBase implements IPlugin{

	public float bannerInitialY = -100, bannerAlpha = 0f, bannerShowTime = 300, bannerShowDelay = 2000;
	
	public ResourceLocation fightbanner = new ResourceLocation("battlemusic", "FIGHT.png");

	
	public FightOverlay(){
		enabled = false;
		breaksCoreLoop = false;
	}
	
	@Override
	public String pluginName() {
		return "Fight Overlay";
	}

	@Override
	public void onRenderTick(Minecraft mc, World world, float partialTick) {
		if(bannerAlpha < 0){
		bannerAlpha = 0f;
	}
	if(bannerAlpha > 1){
		bannerAlpha = 1f;
	}
	if(bannerShowTime == 0){
		//bannerShowDelay--;
		if(bannerShowDelay == 0){
			//bannerShowDelay = 5000;
			//bannerShowTime = 500;
		}
	}
	if(world != null && ProxyClient.mainStream.mainMusic != null){
		if(ProxyClient.mainStream.mainMusic.isFinished || ProxyClient.mainStream.volume > -30){
			bannerShowDelay = 0;
			if(bannerShowDelay == 0){
				bannerShowDelay = 5000;
				bannerShowTime = 500;
			}
		}
		//if(ProxyClient.mainStream.mainMusic.volume > -30){
			//bannerShowTime = 300;
		//}
		if(!ProxyClient.mainStream.mainMusic.isFinished && bannerShowTime > 0){
			bannerShowTime--;
			if(ProxyClient.mainStream.volume > -30){
				if(bannerAlpha < 1f){
				bannerAlpha+=0.01f;
				}
			}else{
				bannerAlpha-=0.01f;
			}
		}else{
			bannerAlpha-=0.01f;
		}
		if(bannerAlpha > 0f && ProxyClient.config.showFightOverlay){
			glPushMatrix();
			float offset = (float)Math.sin((mc.thePlayer.ticksExisted + partialTick)/50)*10;
			float x = -6, y = -50+offset;
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glTranslatef(x+mc.displayWidth/8, y+mc.displayHeight/8, 0f);
			glColor4f(1,1,1,bannerAlpha);
			glTexParameteri( GL_TEXTURE_2D,  GL_TEXTURE_WRAP_S, GL_CLAMP ); 
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP ); 
			mc.getTextureManager().bindTexture(fightbanner);
			int w = 256, h = 256;
			mc.ingameGUI.drawTexturedModalRect(0, 0, 1, 1, w,h);
			glPopMatrix();
		}
	}
	}
	
	
}
