package okushama.battlemusic.plugin;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import okushama.battlemusic.core.ProxyClient;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class GuiPluginManager extends GuiScreen{
	
	public GuiScreen parent;
	
	public GuiPluginManager(GuiScreen p){
		parent = p;
	}
	
	@Override
	public void initGui(){
		int i =0;
		for(IPlugin p : ProxyClient.plugins){
			//for(int j =0;j < 8; j++)
			{
			this.buttonList.add(new GuiButton(i,(width-210),(i*25)+30,p.pluginName()));
			i++;
			}
		}
		this.buttonList.add(new GuiButton(i,width-210, height-30,"Return"));
		super.initGui();
	}

	@Override
	public void drawScreen(int par1, int par2, float par3) {
		this.drawBackground(0);
		this.drawCenteredString(mc.fontRenderer, "Battle Music Plugins", width/2, 10, 0xffffff);
		int i =0;
		for(IPlugin p : ProxyClient.plugins){
			//for(int j =0;j < 8; j++)
			{
				GuiButton btn = (GuiButton)buttonList.get(i);
				this.drawString(mc.fontRenderer, p.pluginName(), 20, (i*25)+35, 0xffffff);
				btn.displayString = p.isEnabled() ? "Enabled" : "Disabled";
			i++;
			}
		}
		super.drawScreen(par1, par2, par3);
	}
	
	

	@Override
	protected void actionPerformed(GuiButton par1GuiButton) {
		if(ProxyClient.plugins.size() > par1GuiButton.id){
			if(ProxyClient.plugins.get(par1GuiButton.id) instanceof PluginBase){
				PluginBase base = (PluginBase)ProxyClient.plugins.get(par1GuiButton.id);
				base.setState(!ProxyClient.plugins.get(par1GuiButton.id).isEnabled());
			}
			return;
		}else{
			mc.displayGuiScreen(parent);
		}
		// TODO Auto-generated method stub
		super.actionPerformed(par1GuiButton);
	}

	@Override
	protected void keyTyped(char par1, int par2) {
		if(par2 == Keyboard.KEY_ESCAPE){
			mc.displayGuiScreen(parent);
		}
		// TODO Auto-generated method stub
	//	super.keyTyped(par1, par2);
	}

	@Override
	protected void mouseClicked(int par1, int par2, int par3) {
		// TODO Auto-generated method stub
		super.mouseClicked(par1, par2, par3);
	}

	@Override
	public void onGuiClosed() {
		// TODO Auto-generated method stub
		super.onGuiClosed();
	}

}
