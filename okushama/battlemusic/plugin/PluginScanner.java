package okushama.battlemusic.plugin;



import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import okushama.battlemusic.Helper;
/* Helper's only function used here is to find the .minecraft dir */


public class PluginScanner {
	
	/** The package found classes should come from */
	private static String packageToScan = "okushama.battlemusic.plugin";
	
	/** The Directory in .minecraft that is scanned for .class or .jar */
	private static String dirToScan = "battlemusic/plugins/";
	
	private static void log(String msg) {
		//System.out.println("PluginScanner: " + msg);	
	}
	
	/** Battle Music plugin list population */
	public static void populatePluginList(List l){
		try{
			for(Package p : Package.getPackages()){
				log("Found Package "+p.getName());
				if(!p.getName().contains(packageToScan)) continue;
				for(Class c : getClassesForPackageFromDir(p, dirToScan)){
					if(c == null){
						log("Nothing Found!");
						continue;
					}
					if(c.getInterfaces().length > 0){
						// Check if class implements plugin interface
						if(c.getInterfaces()[0] == IPlugin.class && c != IPlugin.class){
							// add a new instance to the plugin list
							l.add(c.newInstance());
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private static ArrayList<Class<?>> getClassesForPackageFromDir(Package pkg, String dir) { 
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		
		String pkgname = pkg.getName();
		String relPath = pkgname.replace('.', '/');
	
		File resource = null;
		try {
			resource = Helper.getMinecraftDir(dir);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (resource == null) {
			throw new RuntimeException("Unexpected problem: No resource for " + relPath);
		}
		log("Package: '" + pkgname + "' becomes Resource: '" + resource.toString() + "'");

		resource.getPath();
		if(resource.getName().endsWith("jar")) {
			processJarfile(new File(resource.getPath()), pkgname, classes);
		} else {
			processDirectory(new File(resource.getPath()), pkgname, classes);
		}

		return classes;
	}

	private static Class<?> loadClass(String className) {
		try {
			return Class.forName(className);
		} 
		catch (Exception e) {
			log("Unexpected ClassNotFoundException loading class '" + className + "'");
		}
		return null;
	}

	private static void processDirectory(File directory, String pkgname, ArrayList<Class<?>> classes) {
		log("Reading Directory '" + directory + "'");
		String fix = directory.getAbsolutePath().replace("%20", " ");
		directory = new File(fix);
		String[] files = directory.list();
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i];
			String className = null;
			if (fileName.endsWith(".class")) {
				className = pkgname + '.' + fileName.substring(0, fileName.length() - 6);
			}
			log("FileName '" + fileName + "'  =>  class '"+className+"'");
			if (className != null) {
				classes.add(loadClass(className));
			}
			File subdir = new File(directory, fileName);
			if (subdir.isDirectory()) {
				processDirectory(subdir, pkgname + '.' + fileName, classes);
			}
			if(fileName.endsWith(".jar")){
				try {
					processJarfile(new File(directory.getAbsolutePath()+File.separator, fileName), pkgname, classes);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static void processJarfile(File resource, String pkgname, ArrayList<Class<?>> classes) {
		String relPath = pkgname.replace('.', '/');
		String resPath = resource.getAbsolutePath();
		String jarPath = resPath;
		log("Reading JAR file: '" + jarPath + "'");
		JarFile jarFile = null;
		try {
			jarFile = new JarFile(jarPath);         
		} catch (IOException e) {
			throw new RuntimeException("Unexpected IOException reading JAR File '" + jarPath + "'", e);
		}
		Enumeration<JarEntry> entries = jarFile.entries();
		while(entries.hasMoreElements()) {
			JarEntry entry = entries.nextElement();
			String entryName = entry.getName();
			String className = null;
			if(entryName.endsWith(".class")) {
				className = entryName.replace('/', '.').replace('\\', '.').replace(".class", "");
			}
			log("JarEntry '" + entryName + "'  =>  class '" + className + "'");
			if (className != null) {
				classes.add(loadClass(className));
			}
		}
		try {
			jarFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}