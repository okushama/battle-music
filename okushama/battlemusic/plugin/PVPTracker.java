package okushama.battlemusic.plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.common.event.FMLPreInitializationEvent;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.common.Configuration;
import okushama.battlemusic.ConfigManager;
import okushama.battlemusic.core.BattleStream;
import okushama.battlemusic.core.ProxyClient;

public class PVPTracker extends PluginBase implements IPlugin{

	private boolean triggerAllPlayers = false, triggerVanillaTeamPlayers = false, triggerTestPlayers = false;
	
	public Configuration config = null;
	private List<Entity> loadedEnemies;
	
	public PVPTracker(){
		enabled = true;
		breaksCoreLoop = true;
	}
	
	@Override
	public String pluginName() { return "PVP"; }

		
	@Override
	public void onModInit(FMLPreInitializationEvent evt) {
		config = new Configuration(new File(evt.getSuggestedConfigurationFile().getParentFile(), "battlemusic-plugin-pvp.cfg"));
		loadConfig();
	}
	
	@Override
	public void loadConfig(){
		triggerAllPlayers = config.get("pvp", "Trigger music for all players", false).getBoolean(false);
		triggerVanillaTeamPlayers = config.get("pvp", "Trigger music for opposing Team players", false).getBoolean(false);
		triggerTestPlayers = config.get("pvp", "Trigger test", false).getBoolean(false);
		config.load();
		config.save();
	}
	
	@Override
	public void updateConfig(){
		loadConfig();
		config.get("pvp", "Trigger music for all players", triggerAllPlayers).set(triggerAllPlayers);
		config.get("pvp", "Trigger music for opposing Team players", triggerVanillaTeamPlayers).set(triggerVanillaTeamPlayers);
		config.get("pvp", "Trigger test", triggerTestPlayers).set(triggerTestPlayers);
		config.save();
	}
		
	@Override
	public void onClientTick(Minecraft mc, World w){
		loadedEnemies = new ArrayList<Entity>();
		if(triggerAllPlayers){
			List<Entity> ents = w.loadedEntityList;
			for(int i =0; i < ents.size(); i++){
				if(ents.get(i) instanceof EntityPlayer){
					if(ents.get(i) == mc.thePlayer) continue;
					loadedEnemies.add(ents.get(i));
				}
			}
		}
		if(triggerVanillaTeamPlayers){
			if(mc.thePlayer.getTeam() != null){
				List<Entity> ents = w.loadedEntityList;
				for(int i =0; i < ents.size(); i++){
					if(ents.get(i) instanceof EntityPlayer){
						if(ents.get(i) == mc.thePlayer) continue;
						if(!((EntityPlayer)ents.get(i)).getTeam().isSameTeam(mc.thePlayer.getTeam())){
							loadedEnemies.add(ents.get(i));
						}
					}
				}
			}
		}
		BattleStream stream = ProxyClient.mainStream;
		if(loadedEnemies.size() > 0){
			for(int i = 0; i < loadedEnemies.size(); i++){
				Entity enemy = loadedEnemies.get(i);
				if(enemy.getDistanceSqToEntity(mc.thePlayer) < 200D && mc.thePlayer.canEntityBeSeen(enemy)){
					stream.volumeUp();
					stream.play("overworld.ogg", true);
				}else{
					stream.fadeToStop();
				}
			}
		}else{
			stream.fadeToStop();
		}
	}
}
