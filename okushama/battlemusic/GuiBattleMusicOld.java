package okushama.battlemusic;

import java.awt.Desktop;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import static org.lwjgl.opengl.GL11.*;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;


import okushama.battlemusic.core.ProxyClient;
import okushama.sound.SoundAPI;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureObject;
import net.minecraft.client.resources.DefaultResourcePack;
import net.minecraft.client.resources.Resource;
import net.minecraft.client.resources.ResourcePackFileNotFoundException;
import net.minecraft.client.settings.EnumOptions;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.EnumHelper;

public class GuiBattleMusicOld extends GuiScreen{

	public VolumeSlider volumeSlider;
	public GuiButton saveBtn;
	public int selectionOffset = 0;
	public static int currentSelection = 0;
	public static String result = null;

	public GuiBattleMusicOld(int c){
		ProxyClient.config.loadThemes();
		currentSelection = ProxyClient.config.currentPack;
		selectionOffset = currentSelection;
	}
	
	
	
	
	
	@Override
	public void initGui() {
	//	int width = mc.displayWidth;
	//	int height = mc.displayHeight;
		volumeSlider = new VolumeSlider(0,(width/2)-(80),75,ProxyClient.config.maxVolume+"", ProxyClient.config.maxVolume/80);
		saveBtn = new GuiButton(1, (width/2)-(100), height-40,"Save and Return");
		this.buttonList.add(volumeSlider);
		this.buttonList.add(saveBtn);
		this.buttonList.add(new GuiButton(2, width-20,height/2+10,12,20,">"));
		this.buttonList.add(new GuiButton(3, 10,height/2+10,12,20,"<"));
		
		super.initGui();
	}
	
	
	public void setCurrentTheme(){
		if(ProxyClient.config.currentPack == this.currentSelection){
			return;
		}
		float volume = -70f;
		if(ProxyClient.mainStream.mainMusic != null){
			volume = ProxyClient.mainStream.volume;
			ProxyClient.mainStream.stop(false);
		}
		//System.out.println(ProxyClient.config.themes.get(currentSelection).baseDir);
		ProxyClient.mainStream.currentPack = ProxyClient.config.themes.get(currentSelection).baseDir;
		if(ProxyClient.mainStream.lastTheme.length() > 0)
			ProxyClient.mainStream.play(ProxyClient.mainStream.lastTheme, false);
		if(ProxyClient.mainStream.mainMusic != null){
			ProxyClient.mainStream.volume = volume;
		}
		ProxyClient.config.loadConfig();
		ProxyClient.config.config.get("main", "currentPack",0).set(this.currentSelection);
		ProxyClient.config.currentPack = this.currentSelection;
		ProxyClient.config.config.save();
	}



	public void openThemeBrowser(){
		if(result == null){
		WebReader dp = new WebReader("https://dl.dropboxusercontent.com/u/17362162/Mods/BattleMusic/themeget.txt");
		String test = dp.getLine(0);
		result = test;
		}
		if(result.equals("unavailable")){
			mc.displayGuiScreen(null);
			mc.thePlayer.addChatMessage("\u00A74Unavailable at this time, sorry!");
			return;
		}
		if(result.equals("soon")){
			mc.displayGuiScreen(null);
			mc.thePlayer.addChatMessage("\u00A74Coming soon, Nothing there yet!");
			return;
		}
		String url = result;
		if(Desktop.isDesktopSupported()){
			Desktop d = Desktop.getDesktop();
			try {
				d.browse(new URI(url));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}else{
			mc.displayGuiScreen(null);
			mc.thePlayer.addChatMessage("BattleMusic: \u00A74Could not open Browser!");
			mc.thePlayer.addChatMessage("Visit \u00A7a"+url+"\u00A7f to download theme packs!");
		}
	}


	@Override
	protected void actionPerformed(GuiButton par1GuiButton) {
		if(par1GuiButton.id == 1){
			if(!this.isGetMoreThemesSelected()){
				ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).set(volumeSlider.mainVolume);
				ProxyClient.config.config.save();
				ProxyClient.config.maxVolume = (float)ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).getDouble(80D);
				setCurrentTheme();
				mc.displayGuiScreen(null);
			}else{
				this.openThemeBrowser();
			}
		}
		if(par1GuiButton.id == 2){
			if(currentSelection + 1 < ProxyClient.config.themes.size()){
				currentSelection++;
			}
			if(currentSelection > selectionOffset + 3){
				selectionOffset++;
			}
		}
		if(par1GuiButton.id == 3){
			if(currentSelection > 0){
				currentSelection--;
			}
			if(currentSelection < selectionOffset){
				selectionOffset--;
			}
		}
		super.actionPerformed(par1GuiButton);
	}
	
	public boolean isGetMoreThemesSelected(){
		boolean b= false;
		try{
		if(ProxyClient.config.themes.get(currentSelection) != null){
			if(ProxyClient.config.themes.get(currentSelection) instanceof ThemesGetMore){
				b = true;
			}
		}
		}catch(Exception e){
			currentSelection = 0;
			this.selectionOffset = 0;
		}
		return b;
	}





	@Override
	protected void keyTyped(char par1, int par2) {
		if(par2 == Keyboard.KEY_ESCAPE){
			ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).set(volumeSlider.mainVolume);
			ProxyClient.config.config.save();
			ProxyClient.config.maxVolume = (float)ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).getDouble(80D);
			mc.displayGuiScreen(null);
		}
		if(par2 == Keyboard.KEY_RETURN){
			ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).set(volumeSlider.mainVolume);
			ProxyClient.config.config.save();
			ProxyClient.config.maxVolume = (float)ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).getDouble(80D);
			mc.displayGuiScreen(null);
		}
		super.keyTyped(par1, par2);
	}
	
	@Override
	public void drawScreen(int par1, int par2, float par3) {
		this.drawDefaultBackground();
		if(this.isGetMoreThemesSelected()){
			this.saveBtn.displayString = "Launch Theme Browser";
		}else{
			this.saveBtn.displayString = "Save and Return";
		}
	//	volumeSlider.yPosition = 60;
	//	volumeSlider.xPosition = mc.displayWidth/4-(80);
	//	saveBtn.xPosition = mc.displayWidth/4-(100);
		
		
		
	//	this.drawString(mc.fontRenderer, "Battle Music Options", width/2-(mc.fontRenderer.getStringWidth("Battle Music Options")/2), 20, 0xffffff);
		this.drawString(mc.fontRenderer, "Music Volume:", width/2-(mc.fontRenderer.getStringWidth("Music Volume:")/2), 63, 0xffffff);
		this.drawString(mc.fontRenderer, "Theme Pack Selection:", width/2-(mc.fontRenderer.getStringWidth("Theme Pack Selection:")/2), 106, 0xffffff);

		int offset = width/16-16;
    	Tessellator par5Tessellator = Tessellator.instance;
		
		for(int i = this.selectionOffset; i < ProxyClient.config.themes.size(); i++){
			if(i - this.selectionOffset < 4){
				Theme theme = ProxyClient.config.themes.get(i);
				glPushMatrix();
				try{
			    	glEnable(GL_TEXTURE_2D);
					if(!(theme instanceof ThemesGetMore)){
						glBindTexture(GL_TEXTURE_2D, Helper.loadTexture(theme, "pack.png"));
					}else{
					//	Minecraft.getMinecraft().renderEngine.func_110577_a(theme.image);
					}
			    	glTranslatef(40f+offset,this.height/2,0f);
			    	glPushMatrix();
			    	GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		            par5Tessellator.startDrawingQuads();
		            par5Tessellator.setColorRGBA_F(1f, 1f, 1f, 1f);
		            par5Tessellator.addVertexWithUV(0, 32, 0.0D, 0.0D, 1.0D);
		            par5Tessellator.addVertexWithUV(32, 32, 0.0D, 1.0D, 1.0D);
		            par5Tessellator.addVertexWithUV(32, 0, 0.0D, 1.0D, 0.0D);
		            par5Tessellator.addVertexWithUV(0, 0, 0.0D, 0.0D, 0.0D);
		            par5Tessellator.draw();
		            glPopMatrix();
		            
		            if(i == this.currentSelection){
		            	glPushMatrix();
		            	//Minecraft.getMinecraft().renderEngine.func_110577_a(new ResourceLocation("battlemusic:border.png"));
		            	glTranslatef(-4f,-4f,0f);
		            	GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			            par5Tessellator.startDrawingQuads();
			            par5Tessellator.setColorRGBA_F(1f, 1f, 1f, 1f);
			            par5Tessellator.addVertexWithUV(0, 40, 0.0D, 0.0D, 1.0D);
			            par5Tessellator.addVertexWithUV(40, 40, 0.0D, 1.0D, 1.0D);
			            par5Tessellator.addVertexWithUV(40, 0, 0.0D, 1.0D, 0.0D);
			            par5Tessellator.addVertexWithUV(0, 0, 0.0D, 0.0D, 0.0D);
			            par5Tessellator.draw();
			            glPopMatrix();
		            }
				}catch(Exception e){
					e.printStackTrace();
				}
				int c = 0xffffff;
				if(i == currentSelection){
					c = 0xffff00;
				}
				this.drawString(mc.fontRenderer, theme.name,16-(mc.fontRenderer.getStringWidth(theme.name)/2), 40,c);
				offset += width/4-(10);
			    glPopMatrix();
			}
		}
		glPushMatrix();
    	glEnable(GL_TEXTURE_2D);
		float ioffset = (float)Math.sin((mc.thePlayer.ticksExisted + par3)/50)*10;
		float x = -6, y = -50+ioffset;
		x = width/2-(170.5f); y = 0;
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glTranslatef(x,y,0);
		glScalef(2f,2f,2f);
		//glTranslatef(x+mc.displayWidth/8, y+mc.displayHeight/8, 0f);
		glColor4f(1,1,1,1);
		glTexParameteri( GL_TEXTURE_2D,  GL_TEXTURE_WRAP_S, GL_CLAMP ); 
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP ); 
		ResourceLocation rl = new ResourceLocation("battlemusic", "battlemusicicon.png");
		//mc.renderEngine.func_110577_a(rl);
		int w = 1024, h = 1024;
		w = 256; h = 256;
		//System.out.println("Drawing?");
		this.drawTexturedModalRect(0, 0, 1, 1, w,h);
    	//glDisable(GL_TEXTURE_2D);

		glPopMatrix();
		super.drawScreen(par1, par2, par3);
	}

	@Override
	protected void mouseClicked(int par1, int par2, int par3) {
		super.mouseClicked(par1, par2, par3);
	}

	@Override
	protected void mouseMovedOrUp(int par1, int par2, int par3) {
		super.mouseMovedOrUp(par1, par2, par3);
	}

	@Override
	protected void mouseClickMove(int par1, int par2, int par3, long par4) {
		super.mouseClickMove(par1, par2, par3, par4);
	}

	@Override
	public boolean doesGuiPauseGame() {
		return super.doesGuiPauseGame();
	}
	
	

}
