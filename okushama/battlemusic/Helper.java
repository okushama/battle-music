package okushama.battlemusic;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_RGB;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.imageio.ImageIO;

import okushama.battlemusic.core.ProxyClient;

public class Helper {

	public static InputStream getZipStream(Theme t, String par1Str) throws IOException
	{
	    ZipFile z =/* new ZipFile(t.location);*/ t.zipFile;
	    ZipEntry zipentry = null;
	    InputStream out = null; 
	    try{
	    zipentry = z.getEntry(par1Str);
	    out = z.getInputStream(zipentry);
	    }catch(Exception e){
	    	e.printStackTrace();
	    };
	    //zipfile.close();
	    return out;
	}
	
	public static Theme getCurrentTheme(){
		Theme t = null;
		try{
		t = ProxyClient.config.themes.get(GuiBattleMusicNew.currentSelection);
		}catch(Exception e){
			t =  ProxyClient.config.themes.get(0);
			GuiBattleMusicNew.currentSelection = 0;
		}
		return t;
	}

	public static HashMap<String, Integer>  themeTextures = new HashMap<String,Integer>();
	
	public static int loadTexture(Theme t, String file) throws Exception 
	{
		if(themeTextures.get(t.name) != null){
			return themeTextures.get(t.name);
		}
		BufferedImage tex = null;
		InputStream is = getZipStream(t, file);
	
		try { 
	  		tex = (BufferedImage) ImageIO.read(is); 
	  		if(tex == null) throw new Exception("No pack.png for "+t.location);
		} 
		catch ( Exception e ) { 
			e.printStackTrace();
		} 
		IntBuffer buf = ByteBuffer.allocateDirect(4).order(ByteOrder.nativeOrder()).asIntBuffer();
		ByteBuffer scratch = ByteBuffer.allocateDirect(4 * tex.getWidth() * tex.getHeight());
	  	byte data[] = (byte[]) tex.getRaster().getDataElements(0, 0, tex.getWidth(), tex.getHeight(), null);
	  	scratch.clear();
	  	scratch.put(data);
	  	scratch.rewind();

	  	glGenTextures(buf);  
	  	System.out.println(tex.getTransparency());
	  	if(tex.getTransparency() == 1){
	  		glBindTexture(GL_TEXTURE_2D, buf.get(0));
	  		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	  		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	  		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tex.getWidth(), tex.getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, scratch);
	  	}else{
	  		glBindTexture(GL_TEXTURE_2D, buf.get(0));
	  		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	  		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	  		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex.getWidth(), tex.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, scratch);
	  
	  	}
		themeTextures.put(t.name, buf.get(0));
		return buf.get(0);
	}

	public static File getMinecraftDir(String dir){
		File f = new File("");
		if(f.getName().contains("jars")){
			return new File("../eclipse/Minecraft/bin/"+dir);
		}else{
			return new File(dir);
		}
	}

}
