package okushama.battlemusic.core;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import net.minecraft.client.Minecraft;
import okushama.battlemusic.Helper;
import okushama.sound.Music;
import okushama.sound.Stream;

public class BattleStream extends Stream{

	public BattleStream(String m, String s) {
		super(m, s);
		this.volume = -10f;
		loop = true;
		// TODO Auto-generated constructor stub
	}
	
	public String lastTheme = "";
	public String currentPack = "";
	
	@Override
	public boolean stop(boolean b){
		boolean r = super.stop(b);
		//System.out.println("Stop was called");
		volume = -10f;
		return r;
	}
	
	@Override
	public void run(){
		while(true){
			try{
				if(mainMusic.isFinished && loop){
					play(lastFile, false);
				}
				Thread.sleep(20);
			}catch(Exception e){
				
			}
		}
	}
	
	public void fadeToStop(){
		if(mainMusic == null) return;
		if(!mainMusic.isFinished){
			volumeDown();
			if(volume < -70){
				stop(false);
			}
		}
	}
	
	public void volume(float newVol){
		if(mainMusic == null) return;
		float in = newVol;
		if(in > 0) in = 0;
		if(in < -70) in = -70;
		volume = in;
	}
	
	public void volumeUp(){
		if(mainMusic == null) return;
		if(volume < ProxyClient.config.getMaxVolume()){
			//System.out.println("Volume Increased!");
			volume += 0.5f;
		}
	}
	
	public void volumeDown(){
		if(mainMusic == null) return;
		if(volume > -79){
			//System.out.println("Volume Decreased!");
			volume -= 0.5f;
		}
	}
	
	
	public void play(String file, boolean setVol)
	{		
		//System.out.println(file+" "+lastTheme);

		if(mainMusic != null && !mainMusic.isFinished && file == lastTheme && !loop){
			//System.out.println("Cut off for sanity!");
			//return;
		}
		if(mainMusic != null && file != lastTheme){
			stop(false);
			mainMusic = null;
			
		}
		//volume = -0;
		if(mainMusic == null){
			try {
				if(setVol){
				volume = -50;
				}
			BufferedInputStream in;
			InputStream filein = null;
			try{
				filein = Helper.getZipStream(Helper.getCurrentTheme(), file);
			}catch(Exception e){
			//	e.printStackTrace();
			}
			if(filein == null){
				return;
			}
			in = new BufferedInputStream(filein);	
			//System.out.println("Attempting to play!");	
			if(setVol){
				volume = ProxyClient.config.getMaxVolume();
				}
			mainMusic = new Music(in, this);
			lastTheme = file;
			
			return;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {

		if((mainMusic.isFinished)){
				volume = -70;
				BufferedInputStream in = new BufferedInputStream(Helper.getZipStream(Helper.getCurrentTheme(), file));
				mainMusic = new Music(in, this);
				volume = ProxyClient.config.getMaxVolume();
				lastTheme = file;

			
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
