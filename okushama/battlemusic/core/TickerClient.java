package okushama.battlemusic.core;

import static org.lwjgl.opengl.GL11.*;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;

import okushama.battlemusic.BiomeMusicTest;
import okushama.battlemusic.ConfigManager;
import okushama.battlemusic.plugin.IPlugin;
import okushama.sound.Music;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.glu.GLU;


import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiGameOver;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntityPigZombie;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet11PlayerPosition;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.ObfuscationReflectionHelper;
import cpw.mods.fml.common.TickType;

public class TickerClient
	implements ITickHandler
{
	
	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData) 
	{
	//	System.out.println(Minecraft.getMinecraft().mcDataDir.getAbsolutePath());

		if (type.equals(EnumSet.of(TickType.RENDER)))
		{
			if(Minecraft.getMinecraft().theWorld != null)
			{
				preRenderTick(Minecraft.getMinecraft(), Minecraft.getMinecraft().theWorld, (Float)tickData[0]); //only ingame
			}
		}
	}

	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData) 
	{
        if (type.equals(EnumSet.of(TickType.CLIENT)))
        {
        	if(Minecraft.getMinecraft().theWorld != null)
        	{      		
        		worldTick(Minecraft.getMinecraft(), Minecraft.getMinecraft().theWorld);
        	}else{
        		nullWorldTick(Minecraft.getMinecraft());
        	}
        }
        else if (type.equals(EnumSet.of(TickType.PLAYER)))
        {
        	playerTick((World)((EntityPlayer)tickData[0]).worldObj, (EntityPlayer)tickData[0]);
        }
        else if (type.equals(EnumSet.of(TickType.RENDER)))
        {
        	if(Minecraft.getMinecraft().theWorld != null)
        	{
        		renderTick(Minecraft.getMinecraft(), Minecraft.getMinecraft().theWorld, (Float)tickData[0]); //only ingame
        	}
        }
	}

	@Override
	public EnumSet<TickType> ticks() 
	{
		return EnumSet.of(TickType.CLIENT, TickType.PLAYER, TickType.RENDER);
	}

	@Override
	public String getLabel() 
	{
		return "ClientTickerBattleMusic";
	}
	
	public void nullWorldTick(Minecraft mc){
		BattleStream stream = ProxyClient.mainStream;
		if(stream.mainMusic != null){
			if(!stream.mainMusic.isFinished){
				stream.volume--;
				if(stream.volume < -70){
					stream.stop(false);
				}
			}
		}
	}

	public void worldTick(Minecraft mc, WorldClient world)
	{
		//PVPTracker.onClientTick(mc, world);
	}
	
	public BiomeMusicTest biomeTest = new BiomeMusicTest();
	
	public void playerTick(World world, EntityPlayer player)
	{
		//if(biomeTest.playerTick(world,player)) return;
		
		try{
			BattleStream stream = ProxyClient.mainStream;
			for(IPlugin plugin : ProxyClient.plugins){
				plugin.onClientTick(Minecraft.getMinecraft(), world);
				if(plugin.breakCoreLoop()){
					return;
				}
			}
		if(player.capabilities.isCreativeMode){
			//return;
		}
		if(player.dimension == 1){
			{
				List<Entity> ents = world.loadedEntityList;
				boolean hasDragon = false;
				for(Entity ent : ents){
					if(!ConfigManager.containsMob(EntityList.getEntityString(ent))){
						continue;
					}
					if(ent instanceof net.minecraft.entity.boss.EntityDragon){
						//ent.posX = player.posX;
						//ent.posZ = player.posZ;
						hasDragon = true;
						if(!ProxyClient.config.ignoreDragonDistance){
							stream.volume(ProxyClient.config.getMaxVolume()-((float)player.getDistanceSqToEntity(ent)/1000));
						}else{
							stream.volumeUp();
						}
					}
				}
				if(hasDragon){
				stream.play("dragon.ogg", true);
				if(ProxyClient.config.ignoreDragonDistance){
					stream.volume = ProxyClient.config.getMaxVolume();
				}
				if(stream.volume < -70){
					stream.volume = -70;
				}
				}else{
					if(stream.mainMusic != null){
					stream.volume--;
					if(stream.volume < -70){
					//	stream.stop(true);
					}
					}
				}
			}
			return;
		}
		
		String music = "overworld.ogg";
		if(player.dimension == -1){
			music = "nether.ogg";
		}
		List<Entity> ents = world.loadedEntityList;
		int closest = -1;
		float lowestDist = 200f;
		float minDist = 200f;
		boolean hasWither = false;
		for(Entity ent : ents){
			if(ent instanceof net.minecraft.entity.boss.EntityWither){
				hasWither = true;
				stream.play("wither.ogg", true);
				stream.volume(ProxyClient.config.getMaxVolume()-((float)player.getDistanceSqToEntity(ent)/250));
			//	System.out.println("Wither!");
				return;
			}
		}
	
		
		for(int i = 0; i < ents.size(); i++){
			Entity ent = ents.get(i);
			if(!ConfigManager.containsMob(EntityList.getEntityString(ent))){
				continue;
			}	
			if(ent.isDead) continue;
			if(ent instanceof EntityPigZombie){
				EntityPigZombie pz = (EntityPigZombie)ent;
				int s = ObfuscationReflectionHelper.getPrivateValue(EntityPigZombie.class, pz, "angerLevel", "bs");
				if(s == 0){
					continue;
				}
			}
			if(ent instanceof EntityLivingBase){
				if(((EntityLivingBase)ent).canEntityBeSeen(player))
				if(ent.getDistanceSqToEntity(player) < lowestDist && ent.getDistanceSqToEntity(player) < minDist ){
					lowestDist = ent.getDistanceToEntity(player);
					closest = i;
					//System.out.println(lowestDist);
				}
			}
		}
		if(closest > -1){
			EntityLivingBase ent = (EntityLivingBase)ents.get(closest);	
			if(ent.getDistanceSqToEntity(player) < 200D){
				stream.volumeUp();
				stream.play(music, true);
			}else{
				stream.fadeToStop();
			}
		}else{
			if(stream.mainMusic == null) 
				return;
			if(!stream.mainMusic.isFinished){
				stream.fadeToStop();
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}

	
	public void preRenderTick(Minecraft mc, World world, float renderTick)
	{
	}
	
	
	public void renderTick(Minecraft mc, World world, float renderTick)
	{
		//mc.ingameGUI.drawString(mc.fontRenderer, bannerShowTime+" "+bannerShowDelay, 0, 10, 0xffffff);
		/*if(bannerAlpha < 0){
			bannerAlpha = 0f;
		}
		if(bannerAlpha > 1){
			bannerAlpha = 1f;
		}
		if(bannerShowTime == 0){
			//bannerShowDelay--;
			if(bannerShowDelay == 0){
				//bannerShowDelay = 5000;
				//bannerShowTime = 500;
			}
		}
		if(world != null && ProxyClient.mainStream.mainMusic != null){
			if(ProxyClient.mainStream.mainMusic.isFinished || ProxyClient.mainStream.mainMusic.volume > -30){
				bannerShowDelay = 0;
				if(bannerShowDelay == 0){
					bannerShowDelay = 5000;
					bannerShowTime = 500;
				}
			}
			//if(ProxyClient.mainStream.mainMusic.volume > -30){
				//bannerShowTime = 300;
			//}
			if(!ProxyClient.mainStream.mainMusic.isFinished && bannerShowTime > 0){
				bannerShowTime--;
				if(ProxyClient.mainStream.volume > -30){
					if(bannerAlpha < 1f){
					bannerAlpha+=0.01f;
					}
				}else{
					bannerAlpha-=0.01f;
				}
			}else{
				bannerAlpha-=0.01f;
			}
			if(bannerAlpha > 0f && ProxyClient.config.showFightOverlay){
				glPushMatrix();
				float offset = (float)Math.sin((mc.thePlayer.ticksExisted + renderTick)/50)*10;
				float x = -6, y = -50+offset;
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glTranslatef(x+mc.displayWidth/8, y+mc.displayHeight/8, 0f);
				glColor4f(1,1,1,bannerAlpha);
				glTexParameteri( GL_TEXTURE_2D,  GL_TEXTURE_WRAP_S, GL_CLAMP ); 
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP ); 
				ResourceLocation rl = new ResourceLocation("battlemusic", "FIGHT.png");
				mc.renderEngine.func_110577_a(rl);
				int w = 256, h = 256;
				mc.ingameGUI.drawTexturedModalRect(0, 0, 1, 1, w,h);
				glPopMatrix();
			}
		}*/
	}
}
