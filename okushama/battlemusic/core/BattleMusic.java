package okushama.battlemusic.core;


import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.network.packet.Packet41EntityEffect;
import net.minecraft.network.packet.Packet9Respawn;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.Teleporter;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.*;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLInterModComms;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartedEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppedEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import cpw.mods.fml.common.network.FMLNetworkHandler;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.Player;
import cpw.mods.fml.common.network.NetworkMod.SidedPacketHandler;
import cpw.mods.fml.common.network.NetworkModHandler;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.server.FMLServerHandler;

@Mod(modid = "battlemusic", name = "Battle Music",version = "1.4.2")
@NetworkMod(clientSideRequired = false,serverSideRequired = false)

public class BattleMusic 
{

	@Instance("battlemusic")
	public static BattleMusic instance;
	
	
	
	@SidedProxy(clientSide = "okushama.battlemusic.core.ProxyClient", serverSide = "okushama.battlemusic.core.ProxyCommon")
	public static ProxyCommon proxy;
	
	
	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		proxy.initTickHandlers();
		proxy.initRenderersAndTextures();
		proxy.initSounds();

	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		proxy.postInit();
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		proxy.initMod(event);
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent event)
	{
	}
	
	@EventHandler
	public void serverStarted(FMLServerStartedEvent event)
	{

	}
	
	@EventHandler
	public void serverStopping(FMLServerStoppingEvent event)
	{
		
	}
	
	@EventHandler
	public void serverStopped(FMLServerStoppedEvent event)
	{
		
	}	
	
	@EventHandler
	public void processIMCRequests(FMLInterModComms.IMCEvent event)	
	{
		
	}
   
}
