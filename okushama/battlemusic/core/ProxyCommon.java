package okushama.battlemusic.core;




import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.server.MinecraftServer;

public class ProxyCommon 
{
	
	public TickerServer tickHandlerServer;
	public TickerClient tickHandlerClient;

	public void initCommands(MinecraftServer server)
	{
		
	}
	
	public void postInit(){

	}
 	

 	
 
	public void initMod(FMLPreInitializationEvent event)
	{

	}
	
	
	
	public void initItemRenderer(int itemID) {}
	
	public void initRenderersAndTextures() {}
	
	public void initSounds() {}
	
	public void initTickHandlers() 
	{
		tickHandlerServer = new TickerServer();
		TickRegistry.registerTickHandler(tickHandlerServer, Side.SERVER);
	}

}
