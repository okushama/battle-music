package okushama.battlemusic.core;




import java.util.ArrayList;
import java.util.HashMap;

import okushama.battlemusic.ConfigManager;
import okushama.battlemusic.Keybinds;
import okushama.battlemusic.plugin.IPlugin;
import okushama.battlemusic.plugin.PluginScanner;
import cpw.mods.fml.client.registry.KeyBindingRegistry;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;

public class ProxyClient extends ProxyCommon
{
	
	public static ConfigManager config = new ConfigManager();
	public static BattleStream mainStream = new BattleStream("battlemusic", "main");
	public static HashMap<String, Integer> playerMap = new HashMap<String, Integer>();
	public static ArrayList<IPlugin> plugins = new ArrayList<IPlugin>();

	public void initCommands(MinecraftServer server)
	{
		
	}
	
	public void postInit(){
	}
 	

 	
 
	@Override
	public void initMod(FMLPreInitializationEvent event)
	{
		PluginScanner.populatePluginList(plugins);
		for(IPlugin plugin : plugins){
			plugin.onModInit(event);
		}
		KeyBindingRegistry.registerKeyBinding(new Keybinds());
		//MinecraftForge.EVENT_BUS.register(new Events());
		config.init(event);
	}
	
	
	
	public void initItemRenderer(int itemID) {}
	
	public void initRenderersAndTextures() {}
	
	public void initSounds() {}
	
	public void initTickHandlers() 
	{
		tickHandlerServer = new TickerServer();
		TickRegistry.registerTickHandler(tickHandlerServer, Side.SERVER);
		tickHandlerClient = new TickerClient();
		TickRegistry.registerTickHandler(tickHandlerClient, Side.CLIENT);
	}

}
