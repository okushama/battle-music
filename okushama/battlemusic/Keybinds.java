package okushama.battlemusic;

import java.util.EnumSet;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;


import okushama.battlemusic.core.ProxyClient;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.client.registry.KeyBindingRegistry.KeyHandler;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class Keybinds extends KeyHandler{

	public Minecraft mc = Minecraft.getMinecraft();
	public static KeyBinding openVolume = new KeyBinding("Open Battle Music Options", Keyboard.KEY_B);
	
	public Keybinds() {
		super(new KeyBinding[]{openVolume}, 
				 new boolean[]{false});
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return "JavascriptMod Key Bindings";
	}

	@Override
	public void keyDown(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd, boolean isRepeat) {
		if(kb.keyCode == openVolume.keyCode && tickEnd && mc.currentScreen == null){
			int currentPack = 0;
			for(int i = 0 ; i < ProxyClient.config.themes.size(); i++){
				Theme t = ProxyClient.config.themes.get(i);
				if(!(t instanceof ThemesGetMore)){
					if(t.baseDir.equals(ProxyClient.mainStream.currentPack)){
						currentPack = i;
					}
				}
			}
			mc.displayGuiScreen(new GuiBattleMusicNew(currentPack));
		}
		
	}

	@Override
	public void keyUp(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd) {
		
	}

	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.CLIENT);
	}
	

}
