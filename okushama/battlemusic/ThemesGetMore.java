package okushama.battlemusic;

import net.minecraft.util.ResourceLocation;

public class ThemesGetMore extends Theme{

	public ThemesGetMore() {
		super("", new String[]{"Get More Packs..."});
		this.image = new ResourceLocation("battlemusic:getmore.png");
	}

}
