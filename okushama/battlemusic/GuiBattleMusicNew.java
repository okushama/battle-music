package okushama.battlemusic;

import java.awt.Desktop;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import static org.lwjgl.opengl.GL11.*;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;


import okushama.battlemusic.core.ProxyClient;
import okushama.battlemusic.plugin.GuiPluginManager;
import okushama.sound.SoundAPI;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureObject;
import net.minecraft.client.resources.DefaultResourcePack;
import net.minecraft.client.resources.Resource;
import net.minecraft.client.resources.ResourcePackFileNotFoundException;
import net.minecraft.client.settings.EnumOptions;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.EnumHelper;

public class GuiBattleMusicNew extends GuiScreen{

	public VolumeSlider volumeSlider;
	public GuiButton saveBtn;
	public GuiButton plugins;
	public int selectionOffset = 0;
	public static int currentSelection = 0;
	public static String result = null;
	
	
	public ResourceLocation border = new ResourceLocation("battlemusic:border.png");

	public GuiBattleMusicNew(int c){
		ProxyClient.config.loadThemes();
		currentSelection = ProxyClient.config.currentPack;
		if(currentSelection > 2){
		selectionOffset = currentSelection - 3;
		this.listOffset = selectionOffset*38;
		}else{
			selectionOffset = -1;
			this.listOffset = selectionOffset*38;
		}
	}
	
	
	
	
	
	@Override
	public void initGui() {
	//	int width = mc.displayWidth;
	//	int height = mc.displayHeight;
		volumeSlider = new VolumeSlider(0,(width/2)-(80),65,ProxyClient.config.maxVolume+"", ProxyClient.config.maxVolume/80);
		saveBtn = new GuiButton(1, (width/2)-(100), height-40,"Save and Return");
		plugins = new GuiButton(2,(width/2)-(100), height-50,"Plugins");
		this.buttonList.add(volumeSlider);
		this.buttonList.add(saveBtn);
		this.buttonList.add(plugins);
		//plugins.enabled = false;
		//this.buttonList.add(new GuiButton(2, width-20,height/2+10,12,20,">"));
	//	this.buttonList.add(new GuiButton(3, 10,height/2+10,12,20,"<"));
		
		super.initGui();
	}
	
	
	public void setCurrentTheme(){
		//System.out.println(currentSelection);
		//System.out.println(ProxyClient.config.themes.get(currentSelection).name);
		if(ProxyClient.config.currentPack == this.currentSelection){
			System.out.println("Nope");
			//return;
		}
		float volume = -70f;
		if(ProxyClient.mainStream.mainMusic != null){
			volume = ProxyClient.mainStream.volume;
			ProxyClient.mainStream.stop(false);
		}
		System.out.println(ProxyClient.config.themes.get(currentSelection).baseDir);
		ProxyClient.mainStream.currentPack = ProxyClient.config.themes.get(currentSelection).baseDir;
		if(ProxyClient.mainStream.lastTheme.length() > 0)
			ProxyClient.mainStream.play(ProxyClient.mainStream.lastTheme, false);
		if(ProxyClient.mainStream.mainMusic != null){
			ProxyClient.mainStream.volume = volume;
		}
		ProxyClient.config.loadConfig();
		ProxyClient.config.config.get("main", "currentPack",0).set(this.currentSelection);
		ProxyClient.config.currentPack = this.currentSelection;
		ProxyClient.config.config.save();	
	}



	public void openThemeBrowser(){
		/*if(result != null){
		//WebReader dp = new WebReader("https://dl.dropboxusercontent.com/u/17362162/Mods/BattleMusic/themeget.txt");
		//String test = dp.getLine(0);
		//result = test;
		}
		if(result.equals("unavailable")){
			mc.displayGuiScreen(null);
			mc.thePlayer.addChatMessage("\u00A74Unavailable at this time, sorry!");
			return;
		}
		if(result.equals("soon")){
			mc.displayGuiScreen(null);
			mc.thePlayer.addChatMessage("\u00A74Coming soon, Nothing there yet!");
			return;
		}
		String url = result;*/
		String url = "http://www.coros.us/mods/battlemusic#themes";
		boolean working = true;
		if(Desktop.isDesktopSupported() && working){
			Desktop d = Desktop.getDesktop();
			try {
				d.browse(new URI(url));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}else{
			mc.displayGuiScreen(null);
			mc.thePlayer.addChatMessage("BattleMusic: \u00A74Could not open Browser!");
			mc.thePlayer.addChatMessage("Visit \u00A7a"+url+"\u00A7f to download theme packs!");
		}
	}


	@Override
	protected void actionPerformed(GuiButton par1GuiButton) {
		if(par1GuiButton.id == 1){
			if(!this.isGetMoreThemesSelected()){
				ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).set(volumeSlider.mainVolume);
				ProxyClient.config.config.save();
				ProxyClient.config.maxVolume = (float)ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).getDouble(80D);
				setCurrentTheme();
				mc.displayGuiScreen(null);
			}else{
				this.openThemeBrowser();
			}
		}
		if(par1GuiButton.id == 2){
			mc.displayGuiScreen(new GuiPluginManager(this));
		}

		super.actionPerformed(par1GuiButton);
	}
	
	public boolean isGetMoreThemesSelected(){
		boolean b= false;
		try{
		if(ProxyClient.config.themes.get(currentSelection) != null){
			if(ProxyClient.config.themes.get(currentSelection) instanceof ThemesGetMore){
				b = true;
			}
		}
		}catch(Exception e){
			currentSelection = 0;
			this.selectionOffset = 0;
		}
		return b;
	}





	@Override
	protected void keyTyped(char par1, int par2) {
		if(par2 == Keyboard.KEY_ESCAPE){
			ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).set(volumeSlider.mainVolume);
			ProxyClient.config.config.save();
			ProxyClient.config.maxVolume = (float)ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).getDouble(80D);
			mc.displayGuiScreen(null);
		}
		if(par2 == Keyboard.KEY_RETURN){
			ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).set(volumeSlider.mainVolume);
			ProxyClient.config.config.save();
			ProxyClient.config.maxVolume = (float)ProxyClient.config.config.get("auto","master", (ProxyClient.config.getMaxVolume()+80f)).getDouble(80D);
			mc.displayGuiScreen(null);
		}
		if(listOffset == selectionOffset*38){
		if(par2 == Keyboard.KEY_DOWN){
			if(currentSelection + 1 < ProxyClient.config.themes.size()){
				currentSelection++;
			}
			if(currentSelection > selectionOffset + 2 && currentSelection < ProxyClient.config.themes.size()-2){
				selectionOffset++;
			}
		}
		if(par2 == Keyboard.KEY_UP){
			if(currentSelection > 0){
				currentSelection--;
			}
			if(currentSelection < selectionOffset + 2 && currentSelection > 0){
				selectionOffset--;
			}
		}
		}
		super.keyTyped(par1, par2);
	}
	
	public String f(String t){ return "\u00A7"+t;}
	
	private String[] splitByNumber(String s, int chunkSize){
	    int chunkCount = (s.length() / chunkSize) + (s.length() % chunkSize == 0 ? 0 : 1);
	    String[] returnVal = new String[chunkCount];
	    for(int i=0;i<chunkCount;i++){
	        returnVal[i] = s.substring(i*chunkSize, Math.min((i+1)*chunkSize, s.length()));
	    }
	    return returnVal;
	}
	
	public int listOffset = 0;
	
	@Override
	public void drawScreen(int par1, int par2, float par3) {
	//	System.out.println(width+" "+height);
		//width = 453;
		//height = 240;
	//	this.drawDefaultBackground();
		this.drawBackground(0);
		this.drawRect(12, 0, 175, height, 0xff777777);
		this.drawRect(12, 0, 13, height, 0xff000000);
		this.drawRect(174, 0, 175, height, 0xff000000);
		this.drawRect(13, 0, 14, height, 0xffbbbbbb);
		this.volumeSlider.yPosition = height-90;
		this.volumeSlider.xPosition = width-200;
		this.saveBtn.xPosition = width-mc.fontRenderer.getStringWidth("Save and Return")-140;
		this.plugins.xPosition = width-mc.fontRenderer.getStringWidth("Save and Return")-140;
		this.plugins.yPosition = height-65;
		if(this.isGetMoreThemesSelected()){
			this.saveBtn.displayString = "Launch Theme Browser";
		}else{
			this.saveBtn.displayString = "Save and Return";
		}
	//	volumeSlider.yPosition = 60;
	//	volumeSlider.xPosition = mc.displayWidth/4-(80);
	//	saveBtn.xPosition = mc.displayWidth/4-(100);
		
		
		
	//	this.drawString(mc.fontRenderer, "Battle Music Options", width/2-(mc.fontRenderer.getStringWidth("Battle Music Options")/2), 20, 0xffffff);
		this.drawString(mc.fontRenderer, "Music Volume:", width-125-(mc.fontRenderer.getStringWidth("Music Volume:")/2), height-105, 0xffffff);
		
		Theme currentTheme = ProxyClient.config.themes.get(currentSelection);
		try{
		if(!(currentTheme instanceof ThemesGetMore)){
			this.drawString(mc.fontRenderer, f("n")+"Selected Theme Pack:", width-179-(mc.fontRenderer.getStringWidth("Current Theme Pack:")/2), 45, 0xffffff);
			if(currentTheme.attributes.length > 1){
				this.drawString(mc.fontRenderer, f("l")+"Pack Name:", width-230, 60, 0xffffff);
				this.drawString(mc.fontRenderer, f("l")+"Author:", width-230, 70, 0xffffff);
				this.drawString(mc.fontRenderer, f("l")+"Website:", width-230, 80, 0xffffff);
				this.drawString(mc.fontRenderer, f("l")+"Description:", width-230, 95, 0xffffff);
				this.drawString(mc.fontRenderer, currentTheme.attributes[0], width - 160, 60, 0xffffff);
				this.drawString(mc.fontRenderer, currentTheme.attributes[1], width - 160, 70, 0xffffff);
				this.drawString(mc.fontRenderer, currentTheme.attributes[2], width - 160, 80, 0xffffff);
				String st = currentTheme.attributes[3];
				if(st.length() > 37){
					String[] desc = splitByNumber(st,40);
					for(int i =0; i < desc.length;i++){
						this.drawString(mc.fontRenderer, desc[i], width - 230, 105+(i*10), 0xffffff);
					}
				}else{
					this.drawString(mc.fontRenderer, currentTheme.attributes[3], width - 230, 105, 0xffffff);
				}
			}else{
				this.drawString(mc.fontRenderer, f("l")+"Pack Name:", width-230, 60, 0xffffff);
				this.drawString(mc.fontRenderer, currentTheme.attributes[0], width - 160, 60, 0xffffff);
				this.drawString(mc.fontRenderer, "The theme.okumeta file for this pack", width-220, 100, 0xff0000);
				this.drawString(mc.fontRenderer, "does not contain enough information.", width-220, 110, 0xff0000);
				//this.drawString(mc.fontRenderer, "", width-220, 100, 0xff0000);
			}
		}
		}catch(Exception e){
			
		}
		int offset = 0;
    	Tessellator par5Tessellator = Tessellator.instance;
    	glPushMatrix();
		glTranslatef(0,-(listOffset),0);
		if(listOffset < selectionOffset*38){
			listOffset+=1;
		}else if(listOffset > selectionOffset*38){
			listOffset-=1;
		}
		//System.out.println(height/40);
		for(int i = 0; i < ProxyClient.config.themes.size(); i++){
			//if(listOffset < 0)
		//	System.out.println((ProxyClient.config.themes.size()-2)-currentSelection);
			if(i - this.selectionOffset < height/40+1 && i > selectionOffset-2)
			{
				Theme theme = ProxyClient.config.themes.get(i);
				glPushMatrix();
				try{
			    	glEnable(GL_TEXTURE_2D);
					if(!(theme instanceof ThemesGetMore)){
						glBindTexture(GL_TEXTURE_2D, Helper.loadTexture(theme, "pack.png"));
					}else{
						Minecraft.getMinecraft().getTextureManager().bindTexture(theme.image);
					}
			    	//glTranslatef(40f+offset,this.height/2,0f);
					glTranslatef(20f,10f+(i*38f),0f);
			    	glPushMatrix();
			    	GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		            par5Tessellator.startDrawingQuads();
		            par5Tessellator.setColorRGBA_F(1f, 1f, 1f, 1f);
		            par5Tessellator.addVertexWithUV(0, 32, 0.0D, 0.0D, 1.0D);
		            par5Tessellator.addVertexWithUV(32, 32, 0.0D, 1.0D, 1.0D);
		            par5Tessellator.addVertexWithUV(32, 0, 0.0D, 1.0D, 0.0D);
		            par5Tessellator.addVertexWithUV(0, 0, 0.0D, 0.0D, 0.0D);
		            par5Tessellator.draw();
		            glPopMatrix();
		            
		            if(i == this.currentSelection){
		            	glPushMatrix();
		            	Minecraft.getMinecraft().getTextureManager().bindTexture(border);
		            	glTranslatef(-4f,-4f,0f);
		            	GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			            par5Tessellator.startDrawingQuads();
			            par5Tessellator.setColorRGBA_F(1f, 1f, 1f, 1f);
			            par5Tessellator.addVertexWithUV(0, 40, 0.0D, 0.0D, 1.0D);
			            par5Tessellator.addVertexWithUV(40, 40, 0.0D, 1.0D, 1.0D);
			            par5Tessellator.addVertexWithUV(40, 0, 0.0D, 1.0D, 0.0D);
			            par5Tessellator.addVertexWithUV(0, 0, 0.0D, 0.0D, 0.0D);
			            par5Tessellator.draw();
			            glPopMatrix();
		            }
				}catch(Exception e){
					e.printStackTrace();
				}
				int c = 0xffffff;
				if(i == currentSelection){
					c = 0xffff00;
				}
				//glScalef(0.8f, 0.8f, 0.8f);
				this.drawString(mc.fontRenderer, theme.name,45, 5,c);
				//offset += width/4-(10);
			    glPopMatrix();
			}
		}
		glPopMatrix();
		/*glPushMatrix();
    	glEnable(GL_TEXTURE_2D);
		float ioffset = (float)Math.sin((mc.thePlayer.ticksExisted + par3)/50)*10;
		float x = -6, y = -50+ioffset;
		x = width-60-(170.5f); y = 5;
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glTranslatef(x,y,0);
		glScalef(1.25f,1.25f,1f);
		//glTranslatef(x+mc.displayWidth/8, y+mc.displayHeight/8, 0f);
		glColor4f(1,1,1,1);
		glTexParameteri( GL_TEXTURE_2D,  GL_TEXTURE_WRAP_S, GL_CLAMP ); 
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP ); 
		ResourceLocation rl = new ResourceLocation("battlemusic", "battlemusicicon.png");
		mc.renderEngine.func_110577_a(rl);
		int w = 1024, h = 1024;
		w = 256; h = 256;
		//System.out.println("Drawing?");
		this.drawTexturedModalRect(0, 0, 1, 1, w,h);
    	//glDisable(GL_TEXTURE_2D);

		glPopMatrix();*/
		
		
		this.drawGradientRect(12, height-45, 175, height-35, 0x00000000, 0xff000000);
		this.drawRect(12, height-35, 175, height, 0xff000000);
		this.drawGradientRect(12, 35, 175, 45, 0xff000000, 0x00000000);
		this.drawRect(12, 35, 175, 0, 0xff000000);
		this.drawString(mc.fontRenderer, "Packs", 75, 15, 0xffffff);
	//	this.drawString(mc.fontRenderer, "USE ARROW KEYS TO NAVIGATE", 20, height-25, 0xffffff);
		this.drawString(mc.fontRenderer, f("l")+"Battle Music Options", width-180, 15, 0xffffff);
		super.drawScreen(par1, par2, par3);
		if(lastClickX > 0 && Mouse.isButtonDown(0)){
			this.fontRenderer.drawString("x", (Mouse.getX()/2)-3, height-(Mouse.getY()/2)-5, 0xffffff);
		}
	}

	@Override
	protected void mouseClicked(int par1, int par2, int par3) {
	//	System.out.println(par1+" "+par2+" "+par3);
		if(par1 < 165){
		//	System.out.println(par1+" "+par2);
			for(int i =0; i < 4; i++){
			if(par2 > 50+(i*38) && par2 < 80+(i*38)){
				currentSelection = selectionOffset+1+i;
				int amt = 2;
				if(i == 0 && currentSelection == 0){
					amt = 1;
				}
				selectionOffset = currentSelection-amt;
				//if(selectionOffset < 2){
				//	selectionOffset = 2;
				//}
				if(selectionOffset > ProxyClient.config.themes.size()-5){
				selectionOffset = 3;
				}
			}
			}
		}
		super.mouseClicked(par1, par2, par3);
	}
	
	public int lastClickX = 0, lastClickY = 0;

	@Override
	protected void mouseMovedOrUp(int par1, int par2, int par3) {
	//	System.out.println((width-par1)+" "+(height-par2));
		if(this.listOffset != this.selectionOffset*38){
		//	System.out.println("No can do!");
			return;
		}
		int x = par1;
		int y = par2;
		lastClickX = x;
		lastClickY = y;
		if(x < 160 && x > 30){
			if(y < 180 && y > 170){
			//	System.out.println("U r el");
			}
		}
		super.mouseMovedOrUp(par1, par2, par3);
	}

	@Override
	protected void mouseClickMove(int par1, int par2, int par3, long par4) {
		int x = par1;
		int y = par2;
		if(this.listOffset != this.selectionOffset*38){
			System.out.println("No can do!");
			return;
		}
		//return;/*
		//System.out.println(x+" "+y);
		if(x < 165){
			if(y % 8 == 0)
			{
				if(y < lastClickY){
					if(currentSelection + 1 < ProxyClient.config.themes.size()){
						currentSelection++;
					}
					if(currentSelection > selectionOffset + 2 && currentSelection < ProxyClient.config.themes.size()-2){
						selectionOffset++;
					}
				}else if(y > lastClickY){
					if(currentSelection > 0){
						currentSelection--;
					}
					if(currentSelection < selectionOffset + 2 && currentSelection > 0){
						selectionOffset--;
					}
				}
			}
		}
		lastClickX = x;
		lastClickY = y;
	//	currentSelection = 0;
		super.mouseClickMove(par1, par2, par3, par4);
		//*/
	}

	@Override
	public boolean doesGuiPauseGame() {
		return true;
	}
	
	

}
